// Завдання 2
// Перед вами массив characters, що складається з об'єктів. Кожен об'єкт описує одного персонажа.
// Створіть на його основі масив charactersShortInfo, що складається з об'єктів, у яких є тільки 3-и поля - ім'я, прізвище та вік.

const characters = [
    {
        name: "Елена",
        lastName: "Гилберт",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Кэролайн",
        lastName: "Форбс",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Аларик",
        lastName: "Зальцман",
        age: 31,
        gender: "man",
        status: "human"
    },
    {
        name: "Дэймон",
        lastName: "Сальваторе",
        age: 156,
        gender: "man",
        status: "vampire"
    },
    {
        name: "Ребекка",
        lastName: "Майклсон",
        age: 1089,
        gender: "woman",
        status: "vempire"
    },
    {
        name: "Клаус",
        lastName: "Майклсон",
        age: 1093,
        gender: "man",
        status: "vampire"
    }
];

// 1. Обходимо characters методом map (створюємо новий масив), 
// в кожному елементі початкового масива (який є вкладеним об'єктом), дістаємо три потрібні властивості зі значенням,
// і повертаємо для нового масиву оновлені вкладені об'єкти з потрібними трьома властивостями
// const charactersShortInfo = characters.map(character => {
//     return {
//         name: character.name,
//         lastName: character.lastName,
//         age: character.age
//     }
// });
// console.log("2 task. Новостворений масив: ", charactersShortInfo);


// 2. Переробляємо колбекфункцію в map з використанням метода деконструкції
const charactersShortInfo = characters.map(({ name, lastName, age }) => {
    return {
        name,
        lastName,
        age
    }
});

console.log("2 task. Новостворений масив: ", charactersShortInfo);


// 3. Незвичний синтаксис написання expression в срілковій функції з круглими дужками після => , і без return, чомусь теж працює :)
// const charactersShortInfo = characters.map(({ name, lastName, age }) => ({
//     name,
//     lastName,
//     age
// }));
// console.log("2 task. Новостворений масив: ", charactersShortInfo);
