// Завдання 1
// Дві компанії вирішили об'єднатись, і для цього їм потрібно об'єднати базу даних своїх клієнтів.
// У вас є 2 масиви рядків, у кожному з них – прізвища клієнтів. Створіть на їх основі один масив, 
// який буде об'єднання двох масивів без повторюваних прізвищ клієнтів.

const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];
const clients3 = ["Лондон", "Майклсон", "Пушок", "Бобік"];


// // 1. Спочатку робимо задачу "в лоб":
// const clientsGeneralSet = new Set([...clients1, ...clients2]); // метод Set повертає об'єкт!
// // console.log(clientsGeneralSet);
// const clientsGeneral = [...clientsGeneralSet]; // переробили об'єкт в масив
// // console.log(clientsGeneral);
// const myResult = joinArraysSet(clients1, clients2);
// console.log("1 task. ", myResult);


// // 2. Переробляємо в кастомну функцію, що приймає два масива:
// function joinArraysSet (array1, array2) {
//     const newJoinArraysSetObject = new Set([...array1, ...array2]);
//     // console.log(newJoinArraysSetObject);
//     const newJoinArraysSet = [...newJoinArraysSetObject];
//     // console.log(newJoinArraysSet);
//     return newJoinArraysSet;
// };
// const myResult = joinArraysSet(clients1, clients2);
// console.log("1 task. ", myResult);


// // 3. Переробляємо в універсальну функцію, що приймає безліч масивів (однако з одним рівнем вкладеності):
    function joinArraysSet (...arrays) {

        const newJoinArraysSetObject = new Set(arrays.flat());
        // console.log(newJoinArraysSetObject);
    
        const newJoinArraysSet = [...newJoinArraysSetObject];
        // console.log(newJoinArraysSet);
    
        return newJoinArraysSet;
    };
    
    const myResult = joinArraysSet(clients1, clients2, clients3);
    console.log("1 task. ", myResult);