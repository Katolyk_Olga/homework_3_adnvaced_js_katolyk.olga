// Завдання 6
// Дано об'єкт employee. Додайте до нього властивості age і salary, 
// не змінюючи початковий об'єкт (має бути створено новий об'єкт, 
// який включатиме всі необхідні властивості). Виведіть новий об'єкт у консоль.

const employee = {
  name: 'Vitalii',
  surname: 'Klichko'
}

const newEmployee = {...employee, age: 0, salary: 0};
console.log("6 task. Новий об\'єкт: ", newEmployee);
console.log("6 task. Попередній об\'єкт: ", employee);