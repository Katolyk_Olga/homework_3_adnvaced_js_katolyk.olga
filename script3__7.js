// Завдання 7
// Доповніть код так, щоб він коректно працював

const array = ['value', () => 'showValue'];

// // Допишіть код тут

// // 1. Варіант за індексами з масива
// const value = array[0];
// const showvValue = array[1];


// // 2. Варіант з деконструкцією масива
const [value, showValue] = array;


alert("7 task: ", value); // має бути виведено 'value'
alert("7 task: ", showValue());  // має бути виведено 'showValue'