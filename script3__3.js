// Завдання 3
// У нас є об'єкт' user:

const user1 = {
  name: "John",
  years: 30
};

// Напишіть деструктуруюче присвоєння, яке:
// властивість name присвоїть в змінну ім'я
// властивість years присвоїть в змінну вік
// властивість isAdmin присвоює в змінну isAdmin false, якщо такої властивості немає в об'єкті

// Виведіть змінні на екран.



// 1. За допомогою метода деконструкції задекларували три нові змінні і прсвоїли їм значення з об'єкту user1
// const {name, years, isAdmin = false} = user1;
// console.log(name);
// console.log(years);
// console.log(isAdmin);

// 2. Переприсваюємо змінні новим змінним:
// не змогла в ім'я вставити апостроф, ругається
const {name:імя, years:вік, isAdmin = false} = user1;
console.log("3 task. ", імя);
console.log("3 task. ", вік);
console.log("3 task. ", isAdmin);